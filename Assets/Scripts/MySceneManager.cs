﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MySceneManager : MonoBehaviour {

	public static MySceneManager instance;

	void Awake(){

		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		}
		else
			Destroy (gameObject);

	}

	public void GoToScene(int _buildIndex){

		SceneManager.LoadScene (_buildIndex);

	}

	public bool IsScene(int _buildIndex){

		return SceneManager.GetActiveScene ().buildIndex == _buildIndex;

	}
}
