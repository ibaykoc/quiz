﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizManager : MonoBehaviour {

	[SerializeField] GameObject infoPanel;
	[SerializeField] Text infoText;
	[SerializeField] Button btn;
	[SerializeField] Text btnText;
	[SerializeField] GameObject questionPanel;
	[SerializeField] Text questionText;
	[SerializeField] Text diffText;
	[SerializeField] Text timeText;
	[SerializeField] Text aText;
	[SerializeField] Text bText;
	[SerializeField] Text cText;
	[SerializeField] Text dText;
	Question[] questions;
	Question question;

	int correctNeed = 5;
	int lives = 3;
	WaitForSeconds oneSecWait = new WaitForSeconds (1f);
	int time = 5;
	bool timeTicking = false;

	void Start(){
		switch (GameManager.instance.playerStat.difficulty) {

		case 0:
			diffText.text = "Difficulty : Easy";
			questions = GameManager.instance.easyQuestions;
			break;
		case 1:
			diffText.text = "Difficulty : Medium";
			questions = GameManager.instance.mediumQuestions;
			break;
		case 2:
			diffText.text = "Difficulty : Hard";
			questions = GameManager.instance.hardQuestions;
			break;
		}
		
		ShowInfo ("GET READY!\n"+correctNeed.ToString()+" more correct answer to win\n"+lives.ToString()+" live(s) left","IM READY!");
		btn.onClick.RemoveAllListeners ();
		btn.onClick.AddListener(delegate {
			ShowQuestion();
		});
	}

	Question ChooseQuestion(){
		
		return questions [Random.Range (0, questions.Length)];
		
	}

	public void ShowQuestion(){

		question = ChooseQuestion();
		questionText.text = question.question;
		aText.text = question.choices [0];
		bText.text = question.choices [1];
		cText.text = question.choices [2];
		dText.text = question.choices [3];
		questionPanel.SetActive (true);
		infoPanel.SetActive (false);
		StartCoroutine (IStartTime ());
	}

	public void ShowInfo(string _info, string _buttonText){

		infoText.text = _info;
		btnText.text = _buttonText;
		infoPanel.SetActive (true);
		questionPanel.SetActive (false);
	}

	public void Answer(int _answerIndex){

		if (_answerIndex == question.answerIdx) {
			Correct ();
		} else {
			Incorrect ();
		}
	}

	void Correct(){
		StopCoroutine (IStartTime ());
		timeTicking = false;
		correctNeed--;
		if (correctNeed > 0) {
			ShowInfo ("CORRECT!\n" + correctNeed.ToString () + " more correct answer to win\n" + lives.ToString () + " live(s) left", "NEXT QUESTION");
		}else {	//WIN
			Win();
		}

	}

	void Incorrect(){
		StopCoroutine (IStartTime ());
		timeTicking = false;
		lives--;
		correctNeed = 5;
		if (lives > 0) {
			ShowInfo ("INCORRECT!\n" + correctNeed.ToString () + " more correct answer to win\n" + lives.ToString () + " live(s) left", "NEXT QUESTION");
		}
		else {	//LOSE
			Lose();
		}
	}

	void TimeUp(){
		StopCoroutine (IStartTime ());
		timeTicking = false;
		lives--;
		correctNeed = 5;
		if (lives > 0) {
			ShowInfo ("TIME'S UP!\n" + correctNeed.ToString () + " more correct answer to win\n" + lives.ToString () + " live(s) left", "NEXT QUESTION");
		}
		else {	//LOSE
			Lose();
		}

	}

	void Win(){

		GameManager.instance.PlayerWin();
		btn.onClick.RemoveAllListeners ();
		btn.onClick.AddListener(delegate {
			MySceneManager.instance.GoToScene (1);
		});
		GameSparksManager.instance.SetPlayerData (GameManager.instance.playerStat.win, 
			GameManager.instance.playerStat.lose,
			GameManager.instance.playerStat.winRow,
			GameManager.instance.playerStat.loseRow,
			GameManager.instance.playerStat.difficulty);
		ShowInfo ("YOU WIN!", "GO TO MAIN MENU");

	}

	void Lose(){

		GameManager.instance.PlayerLose();
		btn.onClick.RemoveAllListeners ();
		btn.onClick.AddListener(delegate {
			MySceneManager.instance.GoToScene (1);
		});
		GameSparksManager.instance.SetPlayerData (GameManager.instance.playerStat.win, 
			GameManager.instance.playerStat.lose,
			GameManager.instance.playerStat.winRow,
			GameManager.instance.playerStat.loseRow,
			GameManager.instance.playerStat.difficulty);
		ShowInfo ("YOU LOSE!", "GO TO MAIN MENU");

	}

	IEnumerator IStartTime(){
		time = 5;
		timeText.text = time.ToString ();
		timeTicking = true;

		while (time > 0) {

			yield return oneSecWait;
			if (timeTicking == false)
				yield break;
			time--;
			timeText.text = time.ToString ();

		}

		TimeUp ();

	}

}
