﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginRegister : MonoBehaviour {

	[SerializeField] InputField loginUNInput;
	[SerializeField] InputField loginPWInput;
	[SerializeField] InputField RegDNInput;
	[SerializeField] InputField RegUNInput;
	[SerializeField] InputField RegPWInput;
	[SerializeField] Text infoText;

	public void Register(){
		if (RegDNInput.text == "" || RegUNInput.text == "" || RegPWInput.text == "")
			return;
		infoText.text = "Registering";
		new GameSparks.Api.Requests.RegistrationRequest()
			.SetDisplayName(RegDNInput.text)
			.SetPassword(RegPWInput.text)
			.SetUserName(RegUNInput.text)
			.Send((response) => {
				if (!response.HasErrors) {
					infoText.text =  "Player Registered";
					GameSparksManager.instance.username = RegUNInput.text;
					GameSparksManager.instance.displayName = RegDNInput.text;
					GameSparksManager.instance.password = RegPWInput.text;
					GameSparksManager.instance.SetPlayerData(0,0,0,0,0);
					MySceneManager.instance.GoToScene(1);
				}
				else{
					infoText.text = "Error Registering Player";
				}
			}
			);

	}

	public void Login(){
		if (loginPWInput.text == "" || loginUNInput.text == "")
			return;
		
		new GameSparks.Api.Requests.AuthenticationRequest().SetUserName(loginUNInput.text).SetPassword(loginPWInput.text).Send((response) => {
			if (!response.HasErrors) {
				infoText.text = "Player Authenticated...";
				GameSparksManager.instance.username = loginUNInput.text;
				GameSparksManager.instance.password = loginPWInput.text;
				GameSparksManager.instance.SetDisplayName ();
			} else {
				infoText.text = "Error Authenticating Player...";
			}
		});

	}
}
