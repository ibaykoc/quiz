﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using GameSparks.Api.Requests;
using LitJson;

public class GameSparksManager : MonoBehaviour {

	public static GameSparksManager instance;
	public string username;
	public string displayName;
	public string password;

	void Awake(){

		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		}
		else
			Destroy (gameObject);

	}

	public void SetDisplayName(){

		//We send an AccountDetailsRequest
		new AccountDetailsRequest().Send((response) =>
			{
				//We pass the details we want from our response to the function which will update our information
				displayName = response.DisplayName;
				MySceneManager.instance.GoToScene(1);

			});
		

	}

	public void SetPlayerData(int _win, int _lose, int _winRow, int _loseRow, int _diff){

		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("SAVE_PLAYER")
			.SetEventAttribute("WIN", _win.ToString())
			.SetEventAttribute("LOSE", _lose.ToString())
			.SetEventAttribute("WINROW", _winRow.ToString())
			.SetEventAttribute("LOSEROW", _loseRow.ToString())
			.SetEventAttribute("DIF", _diff.ToString())
			.Send((response) => {
			if (!response.HasErrors) {
				Debug.Log("Player Stat Saved To GameSparks...");
			} else {
				Debug.Log("Error Saving Player Stat...");
			}
		});

	}

//	public void LoadPlayerData(){
//
//		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("LOAD_PLAYER").Send((response) => {
//			if (!response.HasErrors) {
//				Debug.Log("Received Player Stat From GameSparks...");
//				GSData data = response.ScriptData.GetGSData("player_Datas");
//				print("Player ID: " + data.GetString("playerID"));
//				print("Player XP: " + data.GetString("playerXP"));
//				print("Player Gold: " + data.GetString("playerGold"));
//				print("Player Pos: " + data.GetString("playerPos"));
//			} else {
//				Debug.Log("Error Loading Player Stat...");
//			}
//		});
//
//	}

//	public void LoadPlayerStat(){
//
//		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("LOAD_PLAYER").Send((response) => {
//			if (!response.HasErrors) {
//				Debug.Log("Received Player Stat From GameSparks...");
//				GSData data = response.ScriptData.GetGSData("player_Data");
//				PlayerStat playerStat = new PlayerStat();
//				playerStat.win = (int)data.GetInt("playeraWIN");
//				playerStat.lose = (int)data.GetInt("playerLOSE");
////				print("Player ID: " + data.GetString("playerID"));
////				print("Player XP: " + data.GetString("playerXP"));
////				print("Player Gold: " + data.GetString("playerGold"));
////				print("Player Pos: " + data.GetString("playerPos"));
//			} else {
//				Debug.Log("Error Loading Player Data...");
//			}
//		});
//
//	}

//	public void LoadQuestionData(){
//
//		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("LOAD_QUESTIONS").Send((response) => {
//			if (!response.HasErrors) {
//				Debug.Log("Received Player Data From GameSparks...");
//				string data = response.ScriptData.JSON;
//				print(response.ScriptData.JSON);
//				JsonData dataJson = JsonMapper.ToObject(data)[0][0];//Next index 1:easy,2:medium,3:hard
//				print(dataJson[1].Count);
////				print(data.ToString());
//			} else {
//				Debug.Log("Error Loading Player Data...");
//			}
//		});
//
//	}

	public void LoadData(){
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("LOAD_QUESTIONS").Send((response) => {
			if (!response.HasErrors) {
				Debug.Log("Received Question Data From GameSparks...");
				string data = response.ScriptData.JSON;
				print(response.ScriptData.JSON);
				AssignQuestionData(data);
			} else {
				Debug.Log("Error Loading Question Data...");
			}
		});
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("LOAD_PLAYER").Send((response) => {
			if (!response.HasErrors) {
				Debug.Log("Received Player Stat From GameSparks...");
				GSData data = response.ScriptData.GetGSData("player_Data");
				PlayerStat playerStat = new PlayerStat();
				playerStat.win = int.Parse(data.GetString("playerWIN"));
				playerStat.lose = int.Parse(data.GetString("playerLOSE"));
				playerStat.winRow = int.Parse(data.GetString("playerWINROW"));
				playerStat.loseRow = int.Parse(data.GetString("playerLOSEROW"));
				playerStat.difficulty = int.Parse(data.GetString("playerDifficulty"));

				GameManager.instance.playerStat = playerStat;
				//				print("Player ID: " + data.GetString("playerID"));
				//				print("Player XP: " + data.GetString("playerXP"));
				//				print("Player Gold: " + data.GetString("playerGold"));
				//				print("Player Pos: " + data.GetString("playerPos"));
				GameManager.instance.Ready ();
			} else {
				Debug.Log("Error Loading Player Data...");
			}
		});
	}

	public void AssignQuestionData(string data){
		JsonData dataJson;
		dataJson = JsonMapper.ToObject(data)[0][0];//Next index 1:easy,2:medium,3:hard
		//Easy
		GameManager.instance.easyQuestions = new Question[dataJson[1].Count];
		for (int i = 0; i < dataJson[1].Count; i++) {
			GameManager.instance.easyQuestions[i] = new Question(
				dataJson[1][i][0].ToString(),
				dataJson[1][i][1][0].ToString(),
				dataJson[1][i][1][1].ToString(),
				dataJson[1][i][1][2].ToString(),
				dataJson[1][i][1][3].ToString(),
				(int)dataJson[1][i][2]
			);
		}

		//Medium
		GameManager.instance.mediumQuestions = new Question[dataJson[2].Count];
		for (int i = 0; i < dataJson[2].Count; i++) {
			GameManager.instance.mediumQuestions[i] = new Question(
				dataJson[2][i][0].ToString(),
				dataJson[2][i][1][0].ToString(),
				dataJson[2][i][1][1].ToString(),
				dataJson[2][i][1][2].ToString(),
				dataJson[2][i][1][3].ToString(),
				(int)dataJson[2][i][2]
			);
		}

		//Hard
		GameManager.instance.hardQuestions = new Question[dataJson[3].Count];
		for (int i = 0; i < dataJson[3].Count; i++) {
			GameManager.instance.hardQuestions[i] = new Question(
				dataJson[3][i][0].ToString(),
				dataJson[3][i][1][0].ToString(),
				dataJson[3][i][1][1].ToString(),
				dataJson[3][i][1][2].ToString(),
				dataJson[3][i][1][3].ToString(),
				(int)dataJson[3][i][2]
			);
		}
		GameManager.instance.questionLoaded = true;
	}
}


