﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour {

	[SerializeField] Text greetText;
	[SerializeField] Button playButton;

	void Awake(){

		playButton.onClick.AddListener(delegate {
			MySceneManager.instance.GoToScene (2);
		});
		greetText.text = "Hi, " + GameSparksManager.instance.displayName;

	}

	public void Quit(){

		Application.Quit ();

	}

}
