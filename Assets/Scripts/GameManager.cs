﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct Question{

	public string question;
	public string[] choices;
	public int answerIdx;

	public Question (string _question, string _firstChoice, string _secondChoice, string _thirdChoice, string _fourthChoice, int _answerIdx){

		choices = new string[4];
		question = _question;
		choices [0] = _firstChoice;
		choices [1] = _secondChoice;
		choices [2] = _thirdChoice;
		choices [3] = _fourthChoice;
		answerIdx = _answerIdx;

	}

}

[System.Serializable]
public struct PlayerStat{

	public int win;
	public int lose;
	public int winRow;
	public int loseRow;
	public int difficulty; //0:Easy,1:Medium,2:hard

}

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	public Question[] easyQuestions;
	public Question[] mediumQuestions;
	public Question[] hardQuestions;
	public bool questionLoaded = false;
	public PlayerStat playerStat;

	[SerializeField] Text playText;
	[SerializeField] Button playButton;

	[SerializeField] QuizManager quizManager;

	void Awake(){

		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		}
		else
			Destroy (gameObject);

	}

	void Start(){

		if (!questionLoaded) {
			playText.text = "Loading";
			playButton.interactable = false;
			GameSparksManager.instance.LoadData ();
		}
	}

	public void Ready(){

		playText.text = "Play";
		playButton.interactable = true;

	}

	public void PlayerWin(){

		playerStat.win++;
		playerStat.loseRow = 0;
		if(playerStat.difficulty<2)
			playerStat.winRow++;
		if (playerStat.winRow >= 3) {
			//increase difficulty
			playerStat.winRow = 0;
			playerStat.difficulty++;

		}

	}

	public void PlayerLose(){

		playerStat.lose++;
		playerStat.winRow = 0;
		if(playerStat.difficulty>0)
			playerStat.loseRow++;
		if (playerStat.loseRow >= 3) {
			//decrease difficulty
			playerStat.loseRow = 0;
			playerStat.difficulty--;

		}

	}
}
